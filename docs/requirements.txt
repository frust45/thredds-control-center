# SPDX-FileCopyrightText: 2022-2024 Karlsruhe Institute of Technology, Helmholtz-Zentrum hereon GmbH
#
# SPDX-License-Identifier: CC0-1.0

autodocsumm
sphinxcontrib-django
sphinx-design
django-environ
git+https://codebase.helmholtz.cloud/hcdc/hereon-netcdf/sphinxext.git@main#sphinxext
