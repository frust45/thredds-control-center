# SPDX-FileCopyrightText: 2022-2024 Karlsruhe Institute of Technology, Helmholtz-Zentrum hereon GmbH
#
# SPDX-License-Identifier: EUPL-1.2

"""App settings
------------

This module defines the settings options for the
``thredds-control-center`` app.
"""


from __future__ import annotations

import os.path as osp

from django.conf import settings  # noqa: F401

TDS_TOMCAT_URL: str = getattr(
    settings, "TDS_TOMCAT_URL", "http://localhost:8095/"
)

if not TDS_TOMCAT_URL.endswith("/"):
    TDS_TOMCAT_URL += "/"

TDS_TOMCAT_PUBLIC_URL: str = getattr(
    settings, "TDS_TOMCAT_PUBLIC_URL", TDS_TOMCAT_URL
)

if not TDS_TOMCAT_PUBLIC_URL.endswith("/"):
    TDS_TOMCAT_PUBLIC_URL += "/"

TDS_MANAGER_USER: str = getattr(settings, "TDS_MANAGER_USER", "my_user")
TDS_MANAGER_PASSWORD: str = getattr(
    settings, "TDS_MANAGER_PASSWORD", "my_password"
)

TDS_DATA_DIR: str = getattr(
    settings, "TDS_DATA_DIR", "/usr/local/tds/tomcat/content/thredds"
)

TDS_CONTENT_DIR: str = getattr(
    settings, "TDS_CONTENT_DIR", osp.join(TDS_DATA_DIR, "public")
)
