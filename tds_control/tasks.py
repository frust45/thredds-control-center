# SPDX-FileCopyrightText: 2022-2024 Karlsruhe Institute of Technology, Helmholtz-Zentrum hereon GmbH
#
# SPDX-License-Identifier: EUPL-1.2

import requests

from tds_control import app_settings  # noqa: F401

# Restart Thredds according to the Django-Q schedule


def restart_server():
    """Restart the thredds server via the tomcat manager."""
    tomcat_url = app_settings.TDS_TOMCAT_URL
    try:
        response = requests.get(
            f"{tomcat_url}manager/text/reload?path=/thredds",
            auth=(
                app_settings.TDS_MANAGER_USER,
                app_settings.TDS_MANAGER_PASSWORD,
            ),
        )
    except Exception:
        pass

    return response.text
