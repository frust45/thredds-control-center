# SPDX-FileCopyrightText: 2022-2024 Karlsruhe Institute of Technology, Helmholtz-Zentrum hereon GmbH
#
# SPDX-License-Identifier: EUPL-1.2

"""Admin interfaces
----------------

This module defines the thredds-control-center
Admin interfaces.
"""


import nested_admin
from django.contrib import admin  # noqa: F401
from django.contrib.auth.models import Group
from django_q import admin as q_admin
from django_q import models as q_models
from guardian.admin import GuardedModelAdmin
from guardian.shortcuts import assign_perm, get_objects_for_user

from tds_control import models  # noqa: F401

# user = User.objects.create_user('Tony', 'ironman@theavengers.com', 'iamironman')

# # The user has been successfully created
# # We can change its attributes

# user.last_name = 'Stark'
# user.save()
# Register your models here.


class CatalogRefInline(admin.StackedInline):
    """Admin for the CatalogRef model."""

    fk_name = "conn_catalog"
    model = models.CatalogRef
    extra = 0


@admin.register(models.TDSCatalog)
# Using object permission by Django-guardian instead of model permission for TDSCatalog model
class TDSCatalogAdmin(GuardedModelAdmin):
    """Admin for the TDSCatalog model."""

    list_display = ["name", "number_of_entries", "enhanced_catalog"]
    fields = (
        "name",
        "tds4",
        "resolver",
        (
            "odap",
            "dap4",
            "http",
            "wcs",
            "wms",
            "ncssGrid",
            "ncssPoint",
            "cdmremote",
            "iso",
            "ncml",
            "uddc",
        ),
    )
    readonly_fields = ("xml",)

    @admin.display(description="Enhanced Catalog")
    def enhanced_catalog(self, obj):
        """Get the enhanced catalog."""
        return obj.resolver

    def number_of_entries(self, obj: models.TDSCatalog) -> str:
        """Get the number of dataset entries."""
        return str(
            obj.simpledataset_set.count()
            + obj.datasetscan_set.count()
            + obj.aggregationdataset_set.count()
        )

    inlines = [
        CatalogRefInline,
    ]

    # Obtaining catalog access for all users
    def has_module_permission(self, request):
        """Check if the user has module permissions."""
        return True

    # Retrieving catalog querysets for display in the user's catalog list
    def get_queryset(self, request):
        """Get the queryset for the user."""
        if request.user.is_superuser:
            return super().get_queryset(request)

        data = self.get_model_objects_user(request)

        return data

    def get_model_objects_user(self, request, action=None, klass=None):
        """Get the objects for the user."""
        opts = self.opts
        actions = [action] if action else ["add", "view", "edit", "delete"]
        klass = klass if klass else opts.model
        model_name = klass._meta.model_name
        # print(request.user)
        # print(Group.objects.filter(user=request.user)[0])
        return get_objects_for_user(
            user=request.user,
            perms=[f"{perm}_{model_name}" for perm in actions],
            klass=klass,
            any_perm=True,
        )

    # Check the permissions of existing objects and assign permissions to newly-created objects.
    def has_permission(self, request, obj, action):
        """Check if the user has permissions."""
        opts = self.opts
        code_name = f"{action}_{opts.model_name}"
        if obj and (action == "change"):
            code_name_change = f"change_{opts.model_name}"
            code_name_add = f"add_{opts.model_name}"
            code_name_view = f"view_{opts.model_name}"
            code_name_delete = f"delete_{opts.model_name}"
            final_assign = (
                assign_perm(
                    f"{opts.app_label}.{code_name_change}", request.user, obj
                ),
                assign_perm(
                    f"{opts.app_label}.{code_name_add}", request.user, obj
                ),
                assign_perm(
                    f"{opts.app_label}.{code_name_view}", request.user, obj
                ),
                assign_perm(
                    f"{opts.app_label}.{code_name_delete}", request.user, obj
                ),
            )
            if len(Group.objects.filter(user=request.user)) != 0:
                for i in range(len(Group.objects.filter(user=request.user))):
                    final_assign += (
                        assign_perm(
                            f"{opts.app_label}.{code_name_change}",
                            Group.objects.filter(user=request.user)[i],
                            obj,
                        ),
                        assign_perm(
                            f"{opts.app_label}.{code_name_add}",
                            Group.objects.filter(user=request.user)[i],
                            obj,
                        ),
                        assign_perm(
                            f"{opts.app_label}.{code_name_view}",
                            Group.objects.filter(user=request.user)[i],
                            obj,
                        ),
                        assign_perm(
                            f"{opts.app_label}.{code_name_delete}",
                            Group.objects.filter(user=request.user)[i],
                            obj,
                        ),
                    )
            return final_assign

        if obj:
            return request.user.has_perm(f"{opts.app_label}.{code_name}", obj)

        else:
            return self.get_model_objects_user(request).exists()

    # Add permission is enabled for all users
    def has_add_permission(self, request, obj=None):
        """Check if the user has add permissions."""
        return True

    # View permission would be activated if the has_permission() function determined that the user possessed permission.
    def has_view_permission(self, request, obj=None):
        """Check if the user has view permissions."""
        return self.has_permission(request, obj, "view")

    # Change permission would be activated if the has_permission() function determined that the user possessed permission.
    def has_change_permission(self, request, obj=None):
        """Check if the user has change permissions."""
        return self.has_permission(request, obj, "change")

    # Delete permission would be activated if the has_permission() function determined that the user possessed permission.
    def has_delete_permission(self, request, obj=None):
        """Check if the user has delete permissions."""
        return self.has_permission(request, obj, "delete")


class ApplySettingInline(admin.StackedInline):
    """Inline for editing configuration of app_setting and setting of mdoel and tds_control app."""

    model = models.ApplySetting

    extra = 0


@admin.register(models.TDSServer)
# Using object permission by Django-guardian instead of model permission for TDSServer model
class TDSServerAdmin(admin.ModelAdmin):
    """Admin for the TDSServer model."""

    list_display = ["name", "number_of_catalogs"]
    fields = (
        "name",
        "catalogs",
        (
            "cors_activation",
            "cors_maxAge",
            "cors_allowed_methods",
            "cors_allowed_headers",
            "cors_allowed_origin",
        ),
        (
            "netcdf_subset_service_activation",
            "netcdfsubsetservice_scour",
            "netcdfsubsetservice_maxAge",
        ),
        ("opendap_activation", "opendap_ascLimit", "opendap_binLimit"),
        ("wcs_activation", "wcs_allowRemote", "wcs_scour", "wcs_maxAge"),
        (
            "wms_activation",
            "wms_allowRemote",
            "wms_max_image_width",
            "wms_max_image_height",
        ),
        ("ncml_allow", "uddc_allow", "iso_allow"),
    )
    filter_horizontal = ["catalogs"]
    inlines = [ApplySettingInline]
    readonly_fields = ("xml",)

    def number_of_catalogs(self, obj: models.TDSServer):
        """Get the catalogs."""
        return str(obj.catalogs.count())

    # # For showing the objects of catalogs related to the user in M2M variable of TDSServer
    # def get_model_objects_catalog(self, request, action=None, klass=None):
    #     actions = [action] if action else ['view','edit']
    #     klass = klass if klass else models.TDSCatalog
    #     model_name = klass._meta.model_name
    #     return get_objects_for_user(user=request.user, perms=[f'{perm}_{model_name}' for perm in actions], klass=klass, any_perm=True)
    # # Specifying the variables in TDSServer model
    # def get_form(self, request, obj=models.TDSCatalog, **kwargs):
    #     form = super().get_form(request, obj, **kwargs)
    #     form.current_user = request.user
    #     if not super().has_module_permission(request):
    #         disabled_list = ["name","cors_activation","cors_maxAge","cors_allowed_methods","cors_allowed_headers","cors_allowed_origin","netcdf_subset_service_activation","netcdfsubsetservice_scour","netcdfsubsetservice_maxAge","opendap_activation","opendap_ascLimit","opendap_binLimit","wcs_activation","wcs_allowRemote","wcs_scour","wcs_maxAge","wms_activation","wms_allowRemote","wms_max_image_width","wms_max_image_height","ncml_allow","uddc_allow","iso_allow"]
    #         for i in disabled_list:
    #             form.base_fields[i].disabled = True

    #         rangex = self.get_model_objects_catalog(request).count()
    #         form.base_fields['catalogs'].choices = [(self.get_model_objects_catalog(request).values_list('id', flat=True)[indexx] , \
    #              self.get_model_objects_catalog(request).values_list('name', flat=True)[indexx]) for indexx in range(0,rangex)]

    #     return form

    # # Obtaining servers access for all users
    # def has_module_permission(self, request):
    #     if super().has_module_permission(request):
    #         return True
    #     return self.get_model_objects(request).exists()
    # # Retrieving server querysets for display in the user's catalog list
    # def get_queryset(self, request):
    #     if request.user.is_superuser:
    #         return super().get_queryset(request)
    #     data = self.get_model_objects(request)
    #     return data
    # # Accessing model items in the TDSServer module based on Django-Guardian authorization
    # def get_model_objects(self, request, action=None, klass=None):
    #     opts = self.opts
    #     actions = [action] if action else ['view','edit']
    #     klass = klass if klass else opts.model
    #     model_name = klass._meta.model_name
    #     return get_objects_for_user(user=request.user, perms=[f'{perm}_{model_name}' for perm in actions], klass=klass, any_perm=True)

    # # Check the permissions of existing objects and assign permissions to newly-created objects.
    # def has_permission(self, request, obj, action):
    #     opts = self.opts
    #     code_name = f'{action}_{opts.model_name}'
    #     if obj:
    #         return request.user.has_perm(f'{opts.app_label}.{code_name}', obj)
    #     else:
    #         return self.get_model_objects(request).exists()

    # # Add permission is enabled for all users
    # def has_view_permission(self, request, obj=None):
    #     return True

    # # Change permission would be activated if the has_permission() function determined that the user possessed permission.
    # def has_change_permission(self, request, obj=None):
    #     return self.has_permission(request, obj, 'change')


@admin.register(models.SimpleDataset)
# Using object permission by Django-guardian instead of model permission for SimpleDataset model
class SimpleDatasetAdmin(GuardedModelAdmin):
    """Admin for a simple dataset."""

    list_display = ["name", "id", "catalog", "location"]

    list_filter = ["catalog", "data_type", "catalog__tdsserver"]

    search_fields = [
        "name",
        "catalog__name",
        "catalog__tdsserver__name",
        "location",
        "id",
    ]

    readonly_fields = ("xml",)

    # For showing the objects of catalogs related to the user in Foreignkey variable of SimpleDataset
    def get_model_objects_catalog(self, request, action=None, klass=None):
        """Get the model objects in catalogs."""
        actions = [action] if action else ["view", "edit"]
        klass = klass if klass else models.TDSCatalog
        model_name = klass._meta.model_name
        return get_objects_for_user(
            user=request.user,
            perms=[f"{perm}_{model_name}" for perm in actions],
            klass=klass,
            any_perm=True,
        )

    # Specifying the variables in SimpleDataset model
    def get_form(self, request, obj=models.TDSCatalog, **kwargs):
        """Restricting the form according to Guardian setting."""
        form = super().get_form(request, obj, **kwargs)
        form.current_user = request.user

        if not super().has_module_permission(request):
            rangex = self.get_model_objects_catalog(request).count()

            form.base_fields["catalog"].choices = [
                (
                    self.get_model_objects_catalog(request).values_list(
                        "id", flat=True
                    )[indexx],
                    self.get_model_objects_catalog(request).values_list(
                        "name", flat=True
                    )[indexx],
                )
                for indexx in range(0, rangex)
                if not self.get_model_objects_catalog(request).values_list(
                    "resolver", flat=True
                )[indexx]
            ]
        else:
            rangex = self.get_model_objects_catalog(request).count()

            form.base_fields["catalog"].choices = [
                (
                    self.get_model_objects_catalog(request).values_list(
                        "id", flat=True
                    )[indexx],
                    self.get_model_objects_catalog(request).values_list(
                        "name", flat=True
                    )[indexx],
                )
                for indexx in range(0, rangex)
                if not self.get_model_objects_catalog(request).values_list(
                    "resolver", flat=True
                )[indexx]
            ]

        return form

    # Obtaining simple datasets access for all users
    def has_module_permission(self, request):
        """check if the user has permission to access the module."""
        if super().has_module_permission(request):
            return True
        return True

    # Retrieving simple datasets querysets for display in the user's SimpleDataset list
    def get_queryset(self, request):
        """Get the queryset."""
        if request.user.is_superuser:
            return super().get_queryset(request)
        data = self.get_model_objects(request)
        return data

    # Accessing model items in the SimpleDataset module based on Django-Guardian authorization
    def get_model_objects(self, request, action=None, klass=None):
        """Get the model objects."""
        opts = self.opts

        actions = [action] if action else ["add", "view", "edit", "delete"]
        klass = klass if klass else opts.model
        model_name = klass._meta.model_name

        return get_objects_for_user(
            user=request.user,
            perms=[f"{perm}_{model_name}" for perm in actions],
            klass=klass,
            any_perm=True,
        )

    # Check the permissions of existing objects and assign permissions to newly-created objects.
    def has_permission(self, request, obj, action):
        """Check the permission."""
        opts = self.opts
        code_name = f"{action}_{opts.model_name}"
        if obj and (action == "change"):
            code_name_change = f"change_{opts.model_name}"
            code_name_add = f"add_{opts.model_name}"
            code_name_view = f"view_{opts.model_name}"
            code_name_delete = f"delete_{opts.model_name}"
            final_assign = (
                assign_perm(
                    f"{opts.app_label}.{code_name_change}", request.user, obj
                ),
                assign_perm(
                    f"{opts.app_label}.{code_name_add}", request.user, obj
                ),
                assign_perm(
                    f"{opts.app_label}.{code_name_view}", request.user, obj
                ),
                assign_perm(
                    f"{opts.app_label}.{code_name_delete}", request.user, obj
                ),
            )
            if len(Group.objects.filter(user=request.user)) != 0:
                for i in range(len(Group.objects.filter(user=request.user))):
                    final_assign += (
                        assign_perm(
                            f"{opts.app_label}.{code_name_change}",
                            Group.objects.filter(user=request.user)[i],
                            obj,
                        ),
                        assign_perm(
                            f"{opts.app_label}.{code_name_add}",
                            Group.objects.filter(user=request.user)[i],
                            obj,
                        ),
                        assign_perm(
                            f"{opts.app_label}.{code_name_view}",
                            Group.objects.filter(user=request.user)[i],
                            obj,
                        ),
                        assign_perm(
                            f"{opts.app_label}.{code_name_delete}",
                            Group.objects.filter(user=request.user)[i],
                            obj,
                        ),
                    )
            return final_assign

        if obj:
            return request.user.has_perm(f"{opts.app_label}.{code_name}", obj)

        else:
            return self.get_model_objects(request).exists()

    # Add permission is enabled for all users
    def has_add_permission(self, request, obj=None):
        """Check the add permission."""
        return True

    # View permission would be activated if the has_permission() function determined that the user possessed permission.
    def has_view_permission(self, request, obj=None):
        """Check the view permission."""
        return self.has_permission(request, obj, "view")

    # Change permission would be activated if the has_permission() function determined that the user possessed permission.
    def has_change_permission(self, request, obj=None):
        """Check the change permission."""
        return self.has_permission(request, obj, "change")

    # Delete permission would be activated if the has_permission() function determined that the user possessed permission.
    def has_delete_permission(self, request, obj=None):
        """Check the delete permission."""
        return self.has_permission(request, obj, "delete")


@admin.register(models.DatasetScan)
# Using object permission by Django-guardian instead of model permission for DatasetScan model
class DatasetScanAdmin(GuardedModelAdmin):
    """Admin for a datasetScan."""

    list_display = ["name", "id", "catalog", "path", "location"]

    list_filter = ["catalog", "data_type", "catalog__tdsserver"]

    search_fields = [
        "name",
        "catalog__name",
        "catalog__tdsserver__name",
        "location",
        "id",
    ]

    readonly_fields = ("xml",)

    # For showing the objects of catalogs related to the user in Foreignkey variable of DatasetScan
    def get_model_objects_catalog(self, request, action=None, klass=None):
        """Get the model objects."""

        actions = [action] if action else ["view", "edit"]
        klass = klass if klass else models.TDSCatalog
        model_name = klass._meta.model_name
        return get_objects_for_user(
            user=request.user,
            perms=[f"{perm}_{model_name}" for perm in actions],
            klass=klass,
            any_perm=True,
        )

    # Specifying the variables in DatasetScan model
    def get_form(self, request, obj=models.TDSCatalog, **kwargs):
        """Restrict the DatasetScan choices according to the user's permission."""

        form = super().get_form(request, obj, **kwargs)
        form.current_user = request.user

        if not super().has_module_permission(request):
            rangex = self.get_model_objects_catalog(request).count()

            form.base_fields["catalog"].choices = [
                (
                    self.get_model_objects_catalog(request).values_list(
                        "id", flat=True
                    )[indexx],
                    self.get_model_objects_catalog(request).values_list(
                        "name", flat=True
                    )[indexx],
                )
                for indexx in range(0, rangex)
                if not self.get_model_objects_catalog(request).values_list(
                    "resolver", flat=True
                )[indexx]
            ]
        else:
            rangex = self.get_model_objects_catalog(request).count()

            form.base_fields["catalog"].choices = [
                (
                    self.get_model_objects_catalog(request).values_list(
                        "id", flat=True
                    )[indexx],
                    self.get_model_objects_catalog(request).values_list(
                        "name", flat=True
                    )[indexx],
                )
                for indexx in range(0, rangex)
                if not self.get_model_objects_catalog(request).values_list(
                    "resolver", flat=True
                )[indexx]
            ]

        return form

    # Obtaining servers access for all users
    def has_module_permission(self, request):
        """Check the module permission."""

        if super().has_module_permission(request):
            return True
        return True

    # Retrieving datasets querysets for display in the user's DatasetScan list
    def get_queryset(self, request):
        """Get the queryset."""

        if request.user.is_superuser:
            return super().get_queryset(request)
        data = self.get_model_objects(request)
        return data

    # Accessing model items in the DatasetScan module based on Django-Guardian authorization
    def get_model_objects(self, request, action=None, klass=None):
        """Get the model objects."""

        opts = self.opts
        actions = [action] if action else ["add", "view", "edit", "delete"]
        klass = klass if klass else opts.model
        model_name = klass._meta.model_name

        return get_objects_for_user(
            user=request.user,
            perms=[f"{perm}_{model_name}" for perm in actions],
            klass=klass,
            any_perm=True,
        )

    # Check the permissions of existing objects and assign permissions to newly-created objects.
    def has_permission(self, request, obj, action):
        """Check the permission."""
        opts = self.opts
        code_name = f"{action}_{opts.model_name}"
        if obj and (action == "change"):
            code_name_change = f"change_{opts.model_name}"
            code_name_add = f"add_{opts.model_name}"
            code_name_view = f"view_{opts.model_name}"
            code_name_delete = f"delete_{opts.model_name}"
            final_assign = (
                assign_perm(
                    f"{opts.app_label}.{code_name_change}", request.user, obj
                ),
                assign_perm(
                    f"{opts.app_label}.{code_name_add}", request.user, obj
                ),
                assign_perm(
                    f"{opts.app_label}.{code_name_view}", request.user, obj
                ),
                assign_perm(
                    f"{opts.app_label}.{code_name_delete}", request.user, obj
                ),
            )
            if len(Group.objects.filter(user=request.user)) != 0:
                for i in range(len(Group.objects.filter(user=request.user))):
                    final_assign += (
                        assign_perm(
                            f"{opts.app_label}.{code_name_change}",
                            Group.objects.filter(user=request.user)[i],
                            obj,
                        ),
                        assign_perm(
                            f"{opts.app_label}.{code_name_add}",
                            Group.objects.filter(user=request.user)[i],
                            obj,
                        ),
                        assign_perm(
                            f"{opts.app_label}.{code_name_view}",
                            Group.objects.filter(user=request.user)[i],
                            obj,
                        ),
                        assign_perm(
                            f"{opts.app_label}.{code_name_delete}",
                            Group.objects.filter(user=request.user)[i],
                            obj,
                        ),
                    )
            return final_assign

        if obj:
            return request.user.has_perm(f"{opts.app_label}.{code_name}", obj)

        else:
            return self.get_model_objects(request).exists()

    # Add permission is enabled for all users
    def has_add_permission(self, request, obj=None):
        """Check the add permission."""
        return True

    # View permission would be activated if the has_permission() function determined that the user possessed permission.
    def has_view_permission(self, request, obj=None):
        """Check the view permission."""
        return self.has_permission(request, obj, "view")

    # Change permission would be activated if the has_permission() function determined that the user possessed permission.
    def has_change_permission(self, request, obj=None):
        """Check the change permission."""
        return self.has_permission(request, obj, "change")

    # Delete permission would be activated if the has_permission() function determined that the user possessed permission.
    def has_delete_permission(self, request, obj=None):
        """Check the delete permission."""
        return self.has_permission(request, obj, "delete")


# Using object permission by Django-guardian instead of model permission for NetCDFFile inline model
class NetCDFFileInline(admin.StackedInline):
    """Inline for editing FAQs in the category admin."""

    model = models.NetCDFFile

    extra = 0

    # Accessing model items in the NetCDFFile module based on Django-Guardian authorization
    def get_model_objects(self, request, action=None, klass=None):
        """Get the model objects."""
        opts = self.opts
        actions = [action] if action else ["view", "edit"]
        klass = klass if klass else opts.model
        model_name = klass._meta.model_name
        return get_objects_for_user(
            user=request.user,
            perms=[f"{perm}_{model_name}" for perm in actions],
            klass=klass,
            any_perm=True,
        )

    # Obtaining NetCDFFile access for all users
    def has_module_permission(self, request):
        """Check the module permission."""
        return True

    # Check the permissions of existing objects and assign permissions to newly-created objects.
    def has_permission(self, request, obj, action):
        """Check the permission."""
        opts = self.opts
        code_name = f"{action}_{opts.model_name}"
        if obj:
            return request.user.has_perm(f"{opts.app_label}.{code_name}", obj)
        else:
            return self.get_model_objects(request).exists()

    # Add permission is enabled for all users
    def has_add_permission(self, request, obj=None):
        """Check the add permission."""
        return True

    # View permission is enabled for all users
    def has_view_permission(self, request, obj=None):
        """Check the view permission."""
        return True

    # Change permission is enabled for all users
    def has_change_permission(self, request, obj=None):
        """Check the change permission."""
        return True

    # Delete permission is enabled for all users
    def has_delete_permission(self, request, obj=None):
        """Check the delete permission."""
        return True


# Using object permission by Django-guardian instead of model permission for NetCDFScan inline model
class NetCDFScanInline(admin.StackedInline):
    """Inline for editing FAQs in the category admin."""

    model = models.NetCDFScan

    extra = 0

    # Accessing model items in the NetCDFFile module based on Django-Guardian authorization
    def get_model_objects(self, request, action=None, klass=None):
        """Get the model objects."""
        opts = self.opts
        actions = [action] if action else ["view", "edit"]
        klass = klass if klass else opts.model
        model_name = klass._meta.model_name
        return get_objects_for_user(
            user=request.user,
            perms=[f"{perm}_{model_name}" for perm in actions],
            klass=klass,
            any_perm=True,
        )

    # Obtaining NetCDFFile access for all users
    def has_module_permission(self, request):
        """Check the module permission."""
        return True

    # Check the permissions of existing objects and assign permissions to newly-created objects.
    def has_permission(self, request, obj, action):
        """Check the permission."""
        opts = self.opts
        code_name = f"{action}_{opts.model_name}"
        if obj:
            return request.user.has_perm(f"{opts.app_label}.{code_name}", obj)
        else:
            return self.get_model_objects(request).exists()

    # Add permission is enabled for all users
    def has_add_permission(self, request, obj=None):
        """Check the add permission."""
        return True

    # View permission is enabled for all users
    def has_view_permission(self, request, obj=None):
        """Check the view permission."""
        return True

    # Change permission is enabled for all users
    def has_change_permission(self, request, obj=None):
        """Check the change permission."""
        return True

    # Delete permission is enabled for all users
    def has_delete_permission(self, request, obj=None):
        """Check the delete permission."""
        return True


@admin.register(models.AggregationDataset)
# Using object permission by Django-guardian instead of model permission for AggregationDataset model
class AggregationDatasetAdmin(GuardedModelAdmin):
    """Admin for a aggregation dataset."""

    list_display = ["name", "id", "catalog", "url_name", "aggregation_type"]

    list_filter = [
        "catalog",
        "data_type",
        "catalog__tdsserver",
        "aggregation_type",
    ]

    search_fields = [
        "name",
        "catalog__name",
        "catalog__tdsserver__name",
        "url_name",
        "id",
    ]

    inlines = [NetCDFFileInline, NetCDFScanInline]

    readonly_fields = ("xml",)

    # For showing the objects of catalogs related to the user in Foreignkey variable of AggregationDataset
    def get_model_objects_catalog(self, request, action=None, klass=None):
        """Get the model objects."""
        actions = [action] if action else ["view", "edit"]
        klass = klass if klass else models.TDSCatalog
        model_name = klass._meta.model_name
        return get_objects_for_user(
            user=request.user,
            perms=[f"{perm}_{model_name}" for perm in actions],
            klass=klass,
            any_perm=True,
        )

    # Specifying the variables in AggregationDataset model
    def get_form(self, request, obj=models.TDSCatalog, **kwargs):
        """Restrict the catalog choices to the user's catalogs."""
        form = super().get_form(request, obj, **kwargs)
        form.current_user = request.user

        if not super().has_module_permission(request):
            rangex = self.get_model_objects_catalog(request).count()

            form.base_fields["catalog"].choices = [
                (
                    self.get_model_objects_catalog(request).values_list(
                        "id", flat=True
                    )[indexx],
                    self.get_model_objects_catalog(request).values_list(
                        "name", flat=True
                    )[indexx],
                )
                for indexx in range(0, rangex)
                if not self.get_model_objects_catalog(request).values_list(
                    "resolver", flat=True
                )[indexx]
            ]
        else:
            rangex = self.get_model_objects_catalog(request).count()

            form.base_fields["catalog"].choices = [
                (
                    self.get_model_objects_catalog(request).values_list(
                        "id", flat=True
                    )[indexx],
                    self.get_model_objects_catalog(request).values_list(
                        "name", flat=True
                    )[indexx],
                )
                for indexx in range(0, rangex)
                if not self.get_model_objects_catalog(request).values_list(
                    "resolver", flat=True
                )[indexx]
            ]

        return form

    # Obtaining AggregationDataset access for all users
    def has_module_permission(self, request):
        """Check the module permission."""
        if super().has_module_permission(request):
            return True
        return True

    # Retrieving aggregation datasets querysets for display in the user's AggregationDataset list
    def get_queryset(self, request):
        """Get the queryset."""
        if request.user.is_superuser:
            return super().get_queryset(request)
        data = self.get_model_objects(request)
        return data

    # Accessing model items in the AggregationDataset module based on Django-Guardian authorization
    def get_model_objects(self, request, action=None, klass=None):
        """Get the model objects."""
        opts = self.opts
        actions = [action] if action else ["add", "view", "edit", "delete"]
        klass = klass if klass else opts.model
        model_name = klass._meta.model_name

        return get_objects_for_user(
            user=request.user,
            perms=[f"{perm}_{model_name}" for perm in actions],
            klass=klass,
            any_perm=True,
        )

    # Check the permissions of existing objects and assign permissions to newly-created objects.
    def has_permission(self, request, obj, action):
        """Check the permission."""

        opts = self.opts
        code_name = f"{action}_{opts.model_name}"
        if obj and (action == "change"):
            code_name_change = f"change_{opts.model_name}"
            code_name_add = f"add_{opts.model_name}"
            code_name_view = f"view_{opts.model_name}"
            code_name_delete = f"delete_{opts.model_name}"
            final_assign = (
                assign_perm(
                    f"{opts.app_label}.{code_name_change}", request.user, obj
                ),
                assign_perm(
                    f"{opts.app_label}.{code_name_add}", request.user, obj
                ),
                assign_perm(
                    f"{opts.app_label}.{code_name_view}", request.user, obj
                ),
                assign_perm(
                    f"{opts.app_label}.{code_name_delete}", request.user, obj
                ),
            )
            if len(Group.objects.filter(user=request.user)) != 0:
                for i in range(len(Group.objects.filter(user=request.user))):
                    final_assign += (
                        assign_perm(
                            f"{opts.app_label}.{code_name_change}",
                            Group.objects.filter(user=request.user)[i],
                            obj,
                        ),
                        assign_perm(
                            f"{opts.app_label}.{code_name_add}",
                            Group.objects.filter(user=request.user)[i],
                            obj,
                        ),
                        assign_perm(
                            f"{opts.app_label}.{code_name_view}",
                            Group.objects.filter(user=request.user)[i],
                            obj,
                        ),
                        assign_perm(
                            f"{opts.app_label}.{code_name_delete}",
                            Group.objects.filter(user=request.user)[i],
                            obj,
                        ),
                    )
            return final_assign

        if obj:
            return request.user.has_perm(f"{opts.app_label}.{code_name}", obj)

        else:
            return self.get_model_objects(request).exists()

    # Add permission is enabled for all users
    def has_add_permission(self, request, obj=None):
        """Check the add permission."""
        return True

    # View permission would be activated if the has_permission() function determined that the user possessed permission.
    def has_view_permission(self, request, obj=None):
        """Check the view permission."""
        return self.has_permission(request, obj, "view")

    # Change permission would be activated if the has_permission() function determined that the user possessed permission.
    def has_change_permission(self, request, obj=None):
        """Check the change permission."""
        return self.has_permission(request, obj, "change")

    # Delete permission would be activated if the has_permission() function determined that the user possessed permission.
    def has_delete_permission(self, request, obj=None):
        """Check the delete permission."""
        return self.has_permission(request, obj, "delete")


class WMSVariableInline(nested_admin.NestedStackedInline):
    """Inline for editing WMSVariable in the WMSConfig."""

    fields = (
        "name",
        "allow_feature_info",
        "lower_range",
        "upper_range",
        "colorbar",
        "color_bands",
        "log_scaling",
        "interval_time",
    )
    model = models.WMSVariable

    extra = 0


class WMSOverrideInline(nested_admin.NestedStackedInline):
    """Inline for editing WMSOverride in the WMSConfig."""

    fields = (
        ("single_file", "directory"),
        "allow_feature_info",
        "lower_range",
        "upper_range",
        "colorbar",
        "color_bands",
        "log_scaling",
        "interval_time",
    )
    model = models.WMSOverride
    inlines = [WMSVariableInline]
    extra = 0


class WMSStandardNameInline(nested_admin.NestedStackedInline):
    """Inline for editing WMSStandardName in the WMSConfig."""

    fields = (
        "name",
        "unit",
        "allow_feature_info",
        "lower_range",
        "upper_range",
        "colorbar",
        "color_bands",
        "log_scaling",
        "interval_time",
    )
    model = models.WMSStandardName

    extra = 0


@admin.register(models.WMSConfig)
class WMSConfigAdmin(
    nested_admin.NestedModelAdmin,
):
    """Admin for the THREDDS wmsCongig model."""

    list_display = [
        "defaults_allowFeatureInfo",
        "defaults_Lower_range",
        "defaults_Upper_range",
        "defaults_colorbar",
        "defaults_Color_bands",
        "defaults_intervalTime",
        "defaults_logScaling",
    ]
    fields = (
        "defaults_allowFeatureInfo",
        "defaults_Lower_range",
        "defaults_Upper_range",
        "defaults_colorbar",
        "defaults_Color_bands",
        "defaults_intervalTime",
        "defaults_logScaling",
    )
    inlines = [WMSStandardNameInline, WMSOverrideInline]


admin.site.unregister([q_models.Schedule])


@admin.register(q_models.Schedule)
class ChildClassAdmin(q_admin.ScheduleAdmin):
    """Usage of Django query as a scheduler"""

    def get_form(self, request, obj=q_models.Schedule, **kwargs):
        form = super().get_form(request, obj, **kwargs)
        form.base_fields["func"].initial = "tds_control.tasks.restart_server"
        form.base_fields["func"].disabled = True

        return form


class EnhancedDatasetScanVariablesInline(nested_admin.NestedStackedInline):
    """Inline for editing EnhancedDatasetScanVariables in the EnhancedDatasetScan."""

    list_display = [
        "variable_name",
        "veocabulary_name",
        "units",
        "presented_name",
    ]
    fields = (
        "variable_name",
        "veocabulary_name",
        "units",
        "presented_name",
    )
    model = models.EnhancedDatasetScanVariables
    extra = 0

    # Accessing model items in the NetCDFFile module based on Django-Guardian authorization
    def get_model_objects(self, request, action=None, klass=None):
        """Get objects for the given user and action."""

        opts = self.opts
        actions = [action] if action else ["view", "edit"]
        klass = klass if klass else opts.model
        model_name = klass._meta.model_name
        return get_objects_for_user(
            user=request.user,
            perms=[f"{perm}_{model_name}" for perm in actions],
            klass=klass,
            any_perm=True,
        )

    # Obtaining NetCDFFile access for all users
    def has_module_permission(self, request):
        """Check the module permissions."""
        return True

    # Check the permissions of existing objects and assign permissions to newly-created objects.
    def has_permission(self, request, obj, action):
        """Check the permissions."""
        opts = self.opts
        code_name = f"{action}_{opts.model_name}"
        if obj:
            return request.user.has_perm(f"{opts.app_label}.{code_name}", obj)
        else:
            return self.get_model_objects(request).exists()

    # Add permission is enabled for all users
    def has_add_permission(self, request, obj=None):
        """Check the add permission."""
        return True

    # View permission is enabled for all users
    def has_view_permission(self, request, obj=None):
        """Check the view permission."""
        return True

    # Change permission is enabled for all users
    def has_change_permission(self, request, obj=None):
        """Check the change permission."""
        return True

    # Delete permission is enabled for all users
    def has_delete_permission(self, request, obj=None):
        """Check the delete permission."""
        return True


class EnhancedDatasetScanFiltersInline(nested_admin.NestedStackedInline):
    """Inline for editing EnhancedDatasetScanFilters in the EnhancedDatasetScan."""

    list_display = [
        "wildcard",
    ]
    fields = ("wildcard",)
    model = models.EnhancedDatasetScanFilters
    extra = 0
    # Accessing model items in the NetCDFFile module based on Django-Guardian authorization

    def get_model_objects(self, request, action=None, klass=None):
        """Get objects for the given user and action."""
        opts = self.opts
        actions = [action] if action else ["view", "edit"]
        klass = klass if klass else opts.model
        model_name = klass._meta.model_name
        return get_objects_for_user(
            user=request.user,
            perms=[f"{perm}_{model_name}" for perm in actions],
            klass=klass,
            any_perm=True,
        )

    # Obtaining NetCDFFile access for all users
    def has_module_permission(self, request):
        """Check the module permissions."""
        return True

    # Check the permissions of existing objects and assign permissions to newly-created objects.
    def has_permission(self, request, obj, action):
        """Check the permissions."""
        opts = self.opts
        code_name = f"{action}_{opts.model_name}"
        if obj:
            return request.user.has_perm(f"{opts.app_label}.{code_name}", obj)
        else:
            return self.get_model_objects(request).exists()

    # Add permission is enabled for all users
    def has_add_permission(self, request, obj=None):
        """Check the add permission."""
        return True

    # View permission is enabled for all users
    def has_view_permission(self, request, obj=None):
        """Check the view permission."""
        return True

    # Change permission is enabled for all users
    def has_change_permission(self, request, obj=None):
        """Check the change permission."""
        return True

    # Delete permission is enabled for all users
    def has_delete_permission(self, request, obj=None):
        """Check the delete permission."""
        return True


class EnhancedDatasetInline(nested_admin.NestedStackedInline):
    """Inline for editing WMSOverride in the WMSConfig."""

    model = models.EnhancedDataset
    extra = 0

    # Accessing model items in the NetCDFFile module based on Django-Guardian authorization
    def get_model_objects(self, request, action=None, klass=None):
        """Get objects for the given user and action."""
        opts = self.opts
        actions = [action] if action else ["view", "edit"]
        klass = klass if klass else opts.model
        model_name = klass._meta.model_name
        return get_objects_for_user(
            user=request.user,
            perms=[f"{perm}_{model_name}" for perm in actions],
            klass=klass,
            any_perm=True,
        )

    # Obtaining NetCDFFile access for all users
    def has_module_permission(self, request):
        """Check the module permissions."""
        return True

    # Check the permissions of existing objects and assign permissions to newly-created objects.
    def has_permission(self, request, obj, action):
        """Check the permissions."""
        opts = self.opts
        code_name = f"{action}_{opts.model_name}"
        if obj:
            return request.user.has_perm(f"{opts.app_label}.{code_name}", obj)
        else:
            return self.get_model_objects(request).exists()

    # Add permission is enabled for all users
    def has_add_permission(self, request, obj=None):
        """Check the add permission."""
        return True

    # View permission is enabled for all users
    def has_view_permission(self, request, obj=None):
        """Check the view permission."""

        return True

    # Change permission is enabled for all users
    def has_change_permission(self, request, obj=None):
        """Check the change permission."""

        return True

    # Delete permission is enabled for all users
    def has_delete_permission(self, request, obj=None):
        """Check the delete permission."""

        return True


class EnhancedDatasetScanInline(nested_admin.NestedStackedInline):
    """Inline for editing EnhancedDatasetScan in the enhancedCatalog."""

    model = models.EnhancedDatasetScan
    inlines = [
        EnhancedDatasetScanVariablesInline,
        EnhancedDatasetScanFiltersInline,
    ]
    extra = 0

    # Accessing model items in the NetCDFFile module based on Django-Guardian authorization
    def get_model_objects(self, request, action=None, klass=None):
        """Get objects for the given user and action."""

        opts = self.opts
        actions = [action] if action else ["view", "edit"]
        klass = klass if klass else opts.model
        model_name = klass._meta.model_name
        return get_objects_for_user(
            user=request.user,
            perms=[f"{perm}_{model_name}" for perm in actions],
            klass=klass,
            any_perm=True,
        )

    # Obtaining NetCDFFile access for all users
    def has_module_permission(self, request):
        """Check the module permissions."""

        return True

    # Check the permissions of existing objects and assign permissions to newly-created objects.
    def has_permission(self, request, obj, action):
        """Check the permissions."""
        opts = self.opts
        code_name = f"{action}_{opts.model_name}"
        if obj:
            return request.user.has_perm(f"{opts.app_label}.{code_name}", obj)
        else:
            return self.get_model_objects(request).exists()

    # Add permission is enabled for all users
    def has_add_permission(self, request, obj=None):
        """Check the add permission."""

        return True

    # View permission is enabled for all users
    def has_view_permission(self, request, obj=None):
        """Check the view permission."""
        return True

    # Change permission is enabled for all users
    def has_change_permission(self, request, obj=None):
        """Check the change permission."""
        return True

    # Delete permission is enabled for all users
    def has_delete_permission(self, request, obj=None):
        """Check the delete permission."""
        return True


@admin.register(models.EnhancedCatalog)
class EnhancedCatalogAdmin(nested_admin.NestedModelAdmin):
    """Admin for the THREDDS enhancedCatalog model."""

    readonly_fields = ("xml",)
    inlines = [EnhancedDatasetInline, EnhancedDatasetScanInline]

    # For showing the objects of catalogs related to the user in Foreignkey variable of AggregationDataset
    def get_model_objects_catalog(self, request, action=None, klass=None):
        actions = [action] if action else ["view", "edit"]
        klass = klass if klass else models.TDSCatalog
        model_name = klass._meta.model_name
        return get_objects_for_user(
            user=request.user,
            perms=[f"{perm}_{model_name}" for perm in actions],
            klass=klass,
            any_perm=True,
        )

    # Specifying the variables in AggregationDataset model
    def get_form(self, request, obj=models.TDSCatalog, **kwargs):
        """Specify the form to use for the admin page."""

        form = super().get_form(request, obj, **kwargs)
        form.current_user = request.user

        if not super().has_module_permission(request):
            rangex = self.get_model_objects_catalog(request).count()

            form.base_fields["catalog"].choices = [
                (
                    self.get_model_objects_catalog(request).values_list(
                        "id", flat=True
                    )[indexx],
                    self.get_model_objects_catalog(request).values_list(
                        "name", flat=True
                    )[indexx]
                    + "_enahancedCatalog",
                )
                for indexx in range(0, rangex)
                if self.get_model_objects_catalog(request).values_list(
                    "resolver", flat=True
                )[indexx]
            ]
        else:
            rangex = self.get_model_objects_catalog(request).count()

            form.base_fields["catalog"].choices = [
                (
                    self.get_model_objects_catalog(request).values_list(
                        "id", flat=True
                    )[indexx],
                    self.get_model_objects_catalog(request).values_list(
                        "name", flat=True
                    )[indexx]
                    + "_enahancedCatalog",
                )
                for indexx in range(0, rangex)
                if self.get_model_objects_catalog(request).values_list(
                    "resolver", flat=True
                )[indexx]
            ]

        return form

    # Obtaining AggregationDataset access for all users
    def has_module_permission(self, request):
        """Check the module permission."""
        return True

    # Retrieving aggregation datasets querysets for display in the user's AggregationDataset list
    def get_queryset(self, request):
        """Retrieving aggregation datasets querysets for display in the user's AggregationDataset list."""
        if request.user.is_superuser:
            return super().get_queryset(request)
        data = self.get_model_objects(request)
        return data

    # Accessing model items in the AggregationDataset module based on Django-Guardian authorization
    def get_model_objects(self, request, action=None, klass=None):
        """Accessing model items in the AggregationDataset module based on Django-Guardian authorization."""
        opts = self.opts
        actions = [action] if action else ["add", "view", "edit", "delete"]
        klass = klass if klass else opts.model
        model_name = klass._meta.model_name

        return get_objects_for_user(
            user=request.user,
            perms=[f"{perm}_{model_name}" for perm in actions],
            klass=klass,
            any_perm=True,
        )

    # Check the permissions of existing objects and assign permissions to newly-created objects.
    def has_permission(self, request, obj, action):
        """Check the permissions of existing objects and assign permissions to newly-created objects."""
        opts = self.opts
        code_name = f"{action}_{opts.model_name}"
        if obj and (action == "change"):
            code_name_change = f"change_{opts.model_name}"
            code_name_add = f"add_{opts.model_name}"
            code_name_view = f"view_{opts.model_name}"
            code_name_delete = f"delete_{opts.model_name}"
            final_assign = (
                assign_perm(
                    f"{opts.app_label}.{code_name_change}", request.user, obj
                ),
                assign_perm(
                    f"{opts.app_label}.{code_name_add}", request.user, obj
                ),
                assign_perm(
                    f"{opts.app_label}.{code_name_view}", request.user, obj
                ),
                assign_perm(
                    f"{opts.app_label}.{code_name_delete}", request.user, obj
                ),
            )
            if len(Group.objects.filter(user=request.user)) != 0:
                for i in range(len(Group.objects.filter(user=request.user))):
                    final_assign += (
                        assign_perm(
                            f"{opts.app_label}.{code_name_change}",
                            Group.objects.filter(user=request.user)[i],
                            obj,
                        ),
                        assign_perm(
                            f"{opts.app_label}.{code_name_add}",
                            Group.objects.filter(user=request.user)[i],
                            obj,
                        ),
                        assign_perm(
                            f"{opts.app_label}.{code_name_view}",
                            Group.objects.filter(user=request.user)[i],
                            obj,
                        ),
                        assign_perm(
                            f"{opts.app_label}.{code_name_delete}",
                            Group.objects.filter(user=request.user)[i],
                            obj,
                        ),
                    )
            return final_assign

        if obj:
            return request.user.has_perm(f"{opts.app_label}.{code_name}", obj)

        else:
            return self.get_model_objects(request).exists()

    # Add permission is enabled for all users
    def has_add_permission(self, request, obj=None):
        """Check if the user has permission to add the object."""
        return True

    # View permission would be activated if the has_permission() function determined that the user possessed permission.
    def has_view_permission(self, request, obj=None):
        """Check if the user has permission to view the object."""
        return self.has_permission(request, obj, "view")

    # Change permission would be activated if the has_permission() function determined that the user possessed permission.
    def has_change_permission(self, request, obj=None):
        """Check if the user has permission to change the object."""
        return self.has_permission(request, obj, "change")

    # Delete permission would be activated if the has_permission() function determined that the user possessed permission.
    def has_delete_permission(self, request, obj=None):
        """Check if the user has permission to delete the object."""
        return self.has_permission(request, obj, "delete")


@admin.register(models.Creator)
class CreatorAdmin(GuardedModelAdmin):
    """Admin for the Creator model."""


@admin.register(models.Publisher)
class PublisherAdmin(GuardedModelAdmin):
    """Admin for the Publisher model."""


@admin.register(models.Documentation)
class DocumentationAdmin(GuardedModelAdmin):
    """Admin for the Documentation model."""
