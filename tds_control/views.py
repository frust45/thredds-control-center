# SPDX-FileCopyrightText: 2022-2024 Karlsruhe Institute of Technology, Helmholtz-Zentrum hereon GmbH
#
# SPDX-License-Identifier: EUPL-1.2

"""Views
-----

Views of the thredds-control-center app to be imported via the url
config (see :mod:`tds_control.urls`).
"""

from __future__ import annotations

from django.views import generic  # noqa: F401

from tds_control import app_settings  # noqa: F401
from tds_control import models  # noqa: F401
