# SPDX-FileCopyrightText: 2022-2024 Karlsruhe Institute of Technology, Helmholtz-Zentrum hereon GmbH
#
# SPDX-License-Identifier: EUPL-1.2

"""Models
------

Models for the thredds-control-center app.
"""

from __future__ import annotations

import os
import os.path as osp
import uuid
from typing import Union

from django.core import validators
from django.core.validators import MinLengthValidator
from django.db import models  # noqa: F401
from django.db.models.signals import (
    m2m_changed,
    post_delete,
    post_save,
    pre_save,
)
from django.dispatch import receiver
from django.template.loader import render_to_string

from tds_control import app_settings  # noqa: F401

PROJECT_ROOT = os.path.normpath(os.path.dirname(__file__))


class CatalogRef(models.Model):
    """A catalogRef model for a Thredds server."""

    uuid = models.UUIDField(
        primary_key=True,
        default=uuid.uuid4,
        editable=False,
        help_text="The UUID of the catalogRef.",
    )
    conn_catalog = models.ForeignKey(
        "TDSCatalog",
        on_delete=models.CASCADE,
        help_text="is used for showing catalogs name in the administrator of current model",
        related_name="conncatalog",
    )
    catalog = models.ForeignKey(
        "TDSCatalog",
        on_delete=models.CASCADE,
        help_text="is used for getting catalogs name and id from TDSCatalog model",
        related_name="catalog",
    )
    title = models.CharField(
        max_length=200,
        help_text="title of CatalogRef",
    )

    def __str__(self) -> str:
        """Return the string representation of the catalogRef."""
        return str(self.uuid)


class TDSCatalog(models.Model):
    """A catalog for a thredds server."""

    tds4 = models.BooleanField(
        "Thredds data server 4",
        default=False,
        help_text="Check this item when you are using TDS version 4",
    )
    resolver = models.BooleanField(
        "Enhanced Catalog",
        default=False,
        help_text="if using addProxies child element of the datasetScan - enhancedCatalog ",
    )
    odap = models.BooleanField(
        "OpenDAP",
        default=False,
        help_text="activation of OpenDAP",
    )
    dap4 = models.BooleanField(
        "DAP4",
        default=False,
        help_text="activation of DAP4",
    )
    http = models.BooleanField(
        "HTTPServer",
        default=False,
        help_text="activation of HTTPServer",
    )
    wcs = models.BooleanField(
        "WCS",
        default=False,
        help_text="activation of WCS",
    )
    wms = models.BooleanField(
        "WMS",
        default=False,
        help_text="activation of WMS",
    )
    ncssGrid = models.BooleanField(
        "NetcdfSubset",
        default=False,
        help_text="activation of NetcdfSubset",
    )

    ncssPoint = models.BooleanField(
        "NetcdfSubset point",
        default=False,
        help_text="activation of NetcdfSubset point",
    )
    cdmremote = models.BooleanField(
        "CdmRemote",
        default=False,
        help_text="activation of CdmRemote",
    )
    iso = models.BooleanField(
        "ISO",
        default=False,
        help_text="activation of ISO",
    )
    ncml = models.BooleanField(
        "NCML",
        default=False,
        help_text="activation of NCML",
    )
    uddc = models.BooleanField(
        "UDDC",
        default=False,
        help_text="activation of UDDC",
    )

    name = models.SlugField(
        unique=True, help_text="Name of the catalog.", max_length=100
    )

    xml = models.TextField(
        help_text="The rendered XML string of the catalog config.",
        default="",
    )

    def render_xml(self) -> str:
        """Render the catalog as xml."""
        return render_to_string("tds_control/catalog.xml", {"catalog": self})

    @property
    def xml_path(self) -> str:
        """Path to the xml file of the catalog."""
        if self.resolver:
            return osp.join(
                app_settings.TDS_DATA_DIR,
                "catalogs",
                self.name + "_enhancedCatalog" + ".xml",
            )
        else:
            return osp.join(
                app_settings.TDS_DATA_DIR, "catalogs", self.name + ".xml"
            )

    def write_xml(self) -> None:
        """Write the xml file to disk."""
        content = self.render_xml()
        outfile = self.xml_path
        os.makedirs(osp.dirname(outfile), exist_ok=True)
        with open(outfile, "w") as f:
            f.write(content)

    def __str__(self) -> str:
        """rename enhancedcatalog name in the list and xml file"""
        if self.resolver:
            return self.name + "_enhancedCatalog"
        else:
            return self.name


class TDSServer(models.Model):
    """A representation of a thredds server."""

    class CorsMethods(models.TextChoices):
        """Methods that are avaialable for CORS settings."""

        get = "GET", "GET"
        post = "POST", "POST"

    name = models.SlugField(
        unique=True,
        help_text="Name (slug) of the thredds server.",
        max_length=50,
    )
    """Name (slug) of the thredds server."""

    catalogs = models.ManyToManyField(
        TDSCatalog,
        help_text="Root catalogs to be used in the TDS Server",
        blank=True,
    )
    """Root catalogs to be used in the TDS Server"""

    # Settings of CORS
    cors_activation = models.BooleanField(
        "CORS enabled",
        default=True,
        help_text="Allow CORS",
    )
    """Allow CORS"""
    cors_maxAge = models.CharField(
        max_length=200,
        help_text="maxAge of CORS e.g. 1728000",
        default=1728000,
    )
    """maxAge of CORS"""

    cors_allowed_methods = models.CharField(
        max_length=200,
        choices=CorsMethods.choices,
        default="GET",
        help_text="Select Method",
    )
    """allowed Methods of CORS e.g. GET"""
    cors_allowed_headers = models.CharField(
        max_length=200,
        help_text="allowedHeaders of CORS e.g. Authorization",
        default="Authorization",
    )
    """allowed Headers of CORS e.g. Authorization"""
    cors_allowed_origin = models.CharField(
        max_length=200,
        help_text="allowedOrigin of CORS e.g. *",
        default="*",
    )
    """allowed Origin of CORS"""
    # Settings of NetcdfSubsetService

    netcdf_subset_service_activation = models.BooleanField(
        "NetcdfSubsetService allow",
        default=True,
        help_text="Allow NetcdfSubsetService",
    )
    """Allow NetcdfSubsetService"""

    netcdfsubsetservice_scour = models.CharField(
        max_length=200,
        help_text="scour of NetcdfSubsetService e.g. 10 min",
        default="10 min",
    )
    """scour of NetcdfSubsetService e.g. 10 min"""

    netcdfsubsetservice_maxAge = models.CharField(
        max_length=200,
        help_text="maxAge of NetcdfSubsetService e.g. -1 min",
        default="-1 min",
    )
    """maxAge of NetcdfSubsetService e.g. -1 min"""

    # Settings of Opendap
    opendap_activation = models.BooleanField(
        "Opendap allow",
        default=False,
        help_text="Allow Opendap",
    )
    """Allow Opendap"""
    opendap_ascLimit = models.CharField(
        max_length=200, help_text="ascLimit of Opendap e.g. 50", default=50
    )
    """ascLimit of Opendap e.g. 50"""
    opendap_binLimit = models.CharField(
        max_length=200, help_text="binLimit of Opendap e.g. 500", default=500
    )
    """binLimit of Opendap e.g. 500"""

    # Settings of WCS
    wcs_activation = models.BooleanField(
        "WCS allow",
        default=True,
        help_text="Allow WCS",
    )
    """Allow WCS"""
    wcs_allowRemote = models.BooleanField(
        "WCS allowRemote",
        default=False,
        help_text="allowRemote WCS",
    )
    """allowRemote WCS"""
    wcs_scour = models.CharField(
        max_length=200, help_text="scour of WCS e.g. 15 min", default="15 min"
    )
    """scour of WCS e.g. 15 min"""
    wcs_maxAge = models.CharField(
        max_length=200, help_text="maxAge of WCS e.g. 30 min", default="30 min"
    )
    """maxAge of WCS e.g. 30 min"""
    # Settings of WMS
    wms_activation = models.BooleanField(
        "WMS allow",
        default=True,
        help_text="Allow WMS",
    )
    """Allow WMS"""
    wms_allowRemote = models.BooleanField(
        "WMS allowRemote",
        default=False,
        help_text="allowRemote WMS",
    )
    """allowRemote WMS"""
    wms_max_image_width = models.CharField(
        max_length=200,
        help_text="maxImageWidth of WMS e.g. 2048",
        default="2048",
    )
    """maxImageWidth of WMS e.g. 2048"""
    wms_max_image_height = models.CharField(
        max_length=200,
        help_text="maxImageHeight of WMS e.g. 2048",
        default="2048",
    )
    """maxImageHeight of WMS e.g. 2048"""
    # Settings of NCISO
    ncml_allow = models.BooleanField(
        "allow",
        default=True,
        help_text="ncML Allow",
    )
    """ncML Allow"""
    uddc_allow = models.BooleanField(
        "uddcAllow",
        default=True,
        help_text="UDDC Allow ",
    )
    """UDDC Allow """
    iso_allow = models.BooleanField(
        "isoAllow",
        default=True,
        help_text="ISO Allow ",
    )
    """ISO Allow """
    xml = models.TextField(
        help_text="The rendered XML string of the threddsConfig.",
        default="",
    )
    """The rendered XML string of the threddsConfig"""

    def render_xml_app(self) -> str:
        """Render the app_setting."""
        return render_to_string(
            "tds_control/app_settings.xml", {"thredds_app": self}
        )

    @property
    def tds_control_path(self) -> str:
        """Path to the app_setting file of the tds_control."""
        return osp.join(PROJECT_ROOT, "app_settings.py")

    def write_tds_control_app_setting(self) -> None:
        """Write the app_setting file to disk."""
        content = self.render_xml_app()
        outfile = self.tds_control_path

        with open(outfile, "w") as f:
            f.write(content)

    def render_xml(self) -> str:
        """Render the catalog as xml."""
        return render_to_string(
            "tds_control/threddsConfig.xml", {"tdsserver": self}
        )

    def write_xml(self) -> None:
        """Write the xml file to disk."""
        content = self.render_xml()
        outfile = osp.join(app_settings.TDS_DATA_DIR, "threddsConfig.xml")
        with open(outfile, "w") as f:
            f.write(content)

    @property
    def internal_url(self) -> str:
        """The URL where this THREDDS-Server can be accessed."""
        return app_settings.TDS_TOMCAT_URL + self.name

    @property
    def public_url(self) -> str:
        """The URL where this THREDDS-Server can be publicly accessed."""
        return app_settings.TDS_TOMCAT_PUBLIC_URL + self.name

    # def restart_server(self):
    #     """Restart the thredds server via the tomcat manager."""
    #     tomcat_url = app_settings.TDS_TOMCAT_URL
    #     sleep(10)
    #     try:
    #         response = requests.get(
    #             f"{tomcat_url}manager/text/reload?path=/{self.name}",
    #             auth=(
    #                 app_settings.TDS_MANAGER_USER,
    #                 app_settings.TDS_MANAGER_PASSWORD,
    #             ),
    #         )
    #     except Exception:
    #         pass
    #     return response.text

    def __str__(self) -> str:
        """Return the name of the server."""
        return self.name


class TDSSetting(models.Model):
    """Configrations of connection to a thredds server."""

    class Meta:
        """Abstract in Meta class"""

        abstract = True

    tdsserver = models.OneToOneField(
        TDSServer,
        on_delete=models.CASCADE,
        help_text="TDSserver that uses settings",
        default=True,
    )
    """TDSserver that uses settings"""
    tomcat_url_port = models.CharField(
        help_text="The url of connecting to the Tomcat e.g. http://localhost:8006/",
        max_length=50,
    )
    """The url of connecting to the Tomcat e.g. http://localhost:8006/"""

    tomcat_manager_username = models.CharField(
        help_text="The username of connecting to the Tomcat e.g. admin",
        max_length=50,
    )
    """The username of connecting to the Tomcat"""

    tomcat_manager_password = models.CharField(
        help_text="The password of connecting to the Tomcat e.g. 123456",
        max_length=50,
    )
    """The password of connecting to the Tomcat"""

    thredds_content_dir = models.CharField(
        help_text="The content directory of THREDDS e.g. /usr/local/tds/tomcat/content/thredds",
        max_length=50,
    )
    """The content directory of THREDDS e.g. /usr/local/tds/tomcat/content/thredds"""

    def __str__(self) -> str:
        """Return the name of the tomcat url port"""

        return self.tomcat_url_port


class ApplySetting(TDSSetting):
    """Apply configrations of connection to a thredds server."""

    pass


def get_content_dir():
    """Get the content directory of THREDDS."""
    return app_settings.TDS_CONTENT_DIR


class WMSConfig(models.Model):
    """A representation of a thredds server wmsCongig."""

    class ColorBar(models.TextChoices):
        """ColorBar choices."""

        uemep_pm10_palette = "uemep_pm10_palette", "uemep_pm10_palette"
        ferret = "ferret", "ferret"
        alg = "alg", "alg"
        alg2 = "alg2", "alg2"
        uemep_pm10_day_palette = (
            "uemep_pm10_day_palette",
            "uemep_pm10_day_palette",
        )
        metnoprecipitation = "metnoprecipitation", "metnoprecipitation"
        wmo_ice_chart = "wmo_ice_chart", "wmo_ice_chart"
        uemep_pm25_day_palette = (
            "uemep_pm25_day_palette",
            "uemep_pm25_day_palette",
        )
        precipitation = "precipitation", "precipitation"
        occam = "occam", "occam"
        rainbow = "rainbow", "rainbow"
        uemep_no2_palette = "uemep_no2_palette", "uemep_no2_palette"
        metnoredblue = "metnoredblue", "metnoredblue"
        uemep_aqi_palette = "uemep_aqi_palette", "uemep_aqi_palette"
        occam_pastel = "occam_pastel-30", "occam_pastel-30"
        uemep_o3_palette = "uemep_o3_palette", "uemep_o3_palette"
        greyscale = "greyscale", "greyscale"
        ashpalette = "ashpalette", "ashpalette"
        sst_36 = "sst_36", "sst_36"
        ncview = "ncview", "ncview"
        redblue = "redblue", "redblue"
        uemep_pm25_palette = "uemep_pm25_palette", "uemep_pm25_palette"

    defaults_allowFeatureInfo = models.BooleanField(
        "defaults allowFeatureInfo",
        default=True,
        help_text="allow Feature Info",
    )
    """defaults allow FeatureInfo"""
    defaults_Lower_range = models.CharField(
        max_length=200,
        help_text="lower range of variable(default) e.g. -50",
        null=True,
    )
    """defaults Lower range of variable(default) e.g. -50"""
    defaults_Upper_range = models.CharField(
        max_length=200,
        help_text="upper range of variable(default) e.g 50",
        null=True,
    )
    """defaults Upper range of variable(default) e.g 50"""
    defaults_colorbar = models.CharField(
        max_length=200,
        choices=ColorBar.choices,
        default="uemep_pm10_palette",
        help_text="Select colorbar name (default)",
    )
    """defaults Select colorbar name (default)"""
    defaults_Color_bands = models.CharField(
        max_length=200,
        help_text="Number of color bands(default) e.g 20",
        null=True,
    )
    """defaults Number of color bands(default) e.g 20"""
    defaults_logScaling = models.BooleanField(
        "logScaling",
        default=False,
        help_text="log Scaling",
    )
    """defaults log Scaling"""
    defaults_intervalTime = models.BooleanField(
        "intervalTime",
        default=False,
        help_text="interval Time",
    )
    """defaults interval Time"""

    def render_xml(self) -> str:
        """Render the catalog as xml."""
        return render_to_string(
            "tds_control/wmsConfig.xml", {"wmsconfig": self}
        )

    def write_xml(self) -> None:
        """Write the xml file to disk."""
        content = self.render_xml()
        outfile = osp.join(app_settings.TDS_DATA_DIR, "wmsConfig.xml")
        with open(outfile, "w") as f:
            f.write(content)

    def __str__(self) -> str:
        """Return the name of the defaults colorbar."""
        return self.defaults_colorbar


class WMSDatasetPath(models.Model):
    """Base class for an item in WMSConfig model"""

    class Meta:
        """Abatract in Meta class."""

        abstract = True

    allow_feature_info = models.BooleanField(
        "datasetPath allowFeatureInfo",
        default=True,
        help_text="allow Feature Info",
    )
    """datasetPath allow FeatureInfo"""

    lower_range = models.CharField(
        max_length=200,
        help_text="lower range of variable(datasetPath) e.g. -50",
        null=True,
    )
    """datasetPath Lower range of variable(datasetPath) e.g. -50"""

    upper_range = models.CharField(
        max_length=200,
        help_text="upper range of variable(datasetPath) e.g 50",
        null=True,
    )
    """datasetPath Upper range of variable(datasetPath) e.g 50"""

    colorbar = models.CharField(
        max_length=200,
        choices=WMSConfig.ColorBar.choices,
        default="uemep_pm10_palette",
        help_text="Select colorbar name (datasetPath)",
    )
    """datasetPath Select colorbar name (datasetPath)"""

    color_bands = models.CharField(
        max_length=200,
        help_text="Number of color bands(datasetPath) e.g 20",
        null=True,
    )
    """datasetPath Number of color bands(datasetPath) e.g 20"""

    log_scaling = models.BooleanField(
        "logScaling",
        default=False,
        help_text="log Scaling",
    )
    """datasetPath log Scaling"""

    interval_time = models.BooleanField(
        "intervalTime",
        default=False,
        help_text="interval Time",
    )
    """datasetPath interval Time"""
    wms = models.ForeignKey(
        WMSConfig,
        on_delete=models.CASCADE,
        help_text="WMSConfig that uses this netcdf file",
        null=True,
    )
    """WMSConfig that uses this netcdf file"""

    def __str__(self) -> str:
        """Return the name of the colorbar."""
        return self.colorbar


class WMSStandardName(WMSDatasetPath):
    """StandardName parameters in WMSConfig.."""

    name = models.CharField(
        max_length=200, null=True, help_text="Name of variable"
    )
    """Name of variable"""
    unit = models.CharField(
        max_length=200, null=True, help_text="Unit of variable"
    )
    """Unit of variable"""

    def __str__(self) -> str:
        """Return the name of the WMS Standard."""
        return str(self.name)


class WMSOverride(WMSDatasetPath):
    """WMSOverride parameters in WMSConfig."""

    single_file = models.FilePathField(
        path=get_content_dir,
        recursive=True,
        max_length=10000,
        help_text="The location of the files that shall be used",
        null=True,
    )
    """The location of the files that shall be used"""

    directory = models.CharField(
        max_length=200,
        help_text="Direcrtory of model. e.g testAll/*eta_211.nc",
        null=True,
    )
    """Direcrtory of model. e.g testAll/*eta_211.nc"""

    @property
    def relative_path(self) -> str:
        """The :attr:`location` relative to the catalog root location."""
        return osp.relpath(str(self.single_file), get_content_dir())


class WMSVariable(WMSDatasetPath):
    """WMSVariable parameters in WMSConfig."""

    category = models.ForeignKey(WMSOverride, on_delete=models.CASCADE)
    """for connection to WMSOverride in inline feature"""
    name = models.CharField(
        max_length=200, help_text="Name of variable", null=True
    )
    """Name of variable"""

    @property
    def relative_path(self) -> str:
        """The :attr:`location` relative to the catalog root location."""
        return osp.relpath(str(self.name), get_content_dir())


class BaseDataset(models.Model):
    """An entry in a thredds catalog."""

    class Meta:
        """Abatract in Meta class."""

        abstract = True

    class DataType(models.TextChoices):
        """Available data types for datasets."""

        grid = "Grid", "Grid"
        image = "Image", "Image"
        point = "Point", "Point"
        radial = "Radial", "Radial"
        station = "Station", "Station"
        swath = "Swath", "Swath"
        trajectory = "Trajectory", "Trajectory"

    id = models.UUIDField(
        primary_key=True,
        default=uuid.uuid4,
        editable=False,
        help_text="The UUID of the dataset.",
    )
    """The UUID of the dataset."""

    name = models.CharField(
        max_length=200, help_text="The name of the dataset"
    )
    """The name of the dataset"""

    catalog = models.ForeignKey(
        TDSCatalog,
        on_delete=models.CASCADE,
        help_text="The catalog that holds this entry.",
    )
    """The catalog that holds this entry."""

    data_type = models.CharField(
        choices=DataType.choices,
        default=DataType.grid,
        max_length=10,
        help_text="Type of the dataset.",
    )

    xml = models.TextField(
        help_text="The rendered XML string of the dataset config.",
        default="",
    )
    """The rendered XML string of the dataset config."""

    def __str__(self) -> str:
        """Return the name of the dataset."""
        return self.name


class SimpleDataset(BaseDataset):
    """A dataset representing one single file."""

    location = models.FilePathField(
        path=get_content_dir,
        recursive=True,
        max_length=10000,
        help_text="The location of the files that shall be used",
    )

    @property
    def url_path(self) -> str:
        """The `urlPath` attribute for the catalog.

        The `urlPath` attribute is the :attr:`relative_path` of the dataset
        prepended with the catalog :attr:`~TDSCatalog.name`."""

        return self.catalog.name + "/" + self.relative_path

    @property
    def relative_path(self) -> str:
        """The :attr:`location` relative to the catalog root location."""
        print(osp.relpath(self.location, get_content_dir()))
        return osp.relpath(self.location, get_content_dir())

    def render_xml(self) -> str:
        """Render this dataset as `dataset` XML tag."""
        return render_to_string(
            "tds_control/simpledataset.xml", {"dataset": self}
        )


class DatasetScan(BaseDataset):
    """A datasetscan creating multiple datasets with a single entry."""

    path = models.SlugField(
        max_length=50,
        help_text=(
            "The path of the dataset. The final path will be a combination of "
            "the catalog name and this string."
        ),
    )
    """The path of the dataset. The final path will be a combination of the catalog name and this string."""

    location = models.FilePathField(
        path=get_content_dir,
        recursive=True,
        allow_files=False,
        max_length=10000,
        allow_folders=True,
        help_text="The directory to scan.",
    )
    """The directory to files"""

    data_type = models.CharField(  # type: ignore
        choices=BaseDataset.DataType.choices,
        default=BaseDataset.DataType.grid,
        null=True,
        blank=True,
        max_length=10,
        help_text="Type of the dataset.",
    )
    """Type of the dataset."""

    @property
    def relative_path(self) -> str:
        """The :attr:`location` relative to the catalog root location."""
        return osp.relpath(self.location, get_content_dir())

    def render_xml(self) -> str:
        """Render this dataset as `datasetScan` XML tag."""
        return render_to_string(
            "tds_control/datasetscan.xml", {"dataset": self}
        )


class AggregationDataset(BaseDataset):
    """An dataset representing the aggregation of multiple files."""

    class AggregationType(models.TextChoices):
        """The type of aggregations."""

        union = (
            "union",
            (
                "Combine multiple files with different variables but same "
                "coordinates (union)"
            ),
        )
        join_existing = (
            "JoinExisting",
            "Join multiple files along an existing dimension (JoinExisting)",
        )
        join_new = (
            "JoinNew",
            "Create a new dimension and join multiple files (JoinNew)",
        )

    aggregation_type = models.CharField(
        max_length=12,
        choices=AggregationType.choices,
        help_text="Aggregation type specifying how to combine the files.",
    )
    """Aggregation type specifying how to combine the files."""

    dimension_name = models.CharField(
        max_length=50,
        help_text=(
            "Name of the dimension along we are supposed to join (only "
            "relevant for JoinNew and JoinExisting)."
        ),
    )
    """Name of the dimension along we are supposed to join (only relevant for JoinNew and JoinExisting)."""

    url_name = models.SlugField(
        max_length=100, help_text="Name of the resulting dataset."
    )
    """Name of the resulting dataset."""

    @property
    def url_path(self) -> str:
        """The `urlPath` attribute for the catalog.

        The `urlPath` attribute is the :attr:`relative_path` of the dataset
        prepended with the catalog :attr:`~TDSCatalog.name`."""
        return self.url_name

    def render_xml(self) -> str:
        """Render this dataset as `dataset` XML tag."""

        return render_to_string(
            "tds_control/aggregationdataset.xml", {"dataset": self}
        )


class NetCDFBase(models.Model):
    """Base class for an item for an aggregation dataset."""

    class Meta:
        """Meta options for the model."""

        abstract = True

    aggregation = models.ForeignKey(
        AggregationDataset,
        on_delete=models.CASCADE,
        help_text="Aggregation that uses this netcdf file",
    )
    """Aggregation that uses this netcdf file"""

    location = models.FilePathField(
        path=get_content_dir,
        recursive=True,
        max_length=10000,
        help_text="The location of the files that shall be used",
    )
    """The location of the files that shall be used"""

    @property
    def relative_path(self) -> str:
        """The :attr:`location` relative to the catalog root location."""
        return osp.relpath(self.location, get_content_dir())


class NetCDFFile(NetCDFBase):
    """An explicit location of a netcdf file to be used in an aggregation."""

    pass


class NetCDFScan(NetCDFBase):
    """A scan to add multiple aggregations."""

    location = models.FilePathField(
        path=get_content_dir,
        recursive=True,
        max_length=10000,
        allow_files=False,
        allow_folders=True,
        help_text="The location of the folders that shall be scanned",
    )
    """The location of the folders that shall be scanned"""

    regexp = models.CharField(
        max_length=10000,
        default=r".*\.nc$",
        help_text="The regular expression to use for finding files.",
    )
    """The regular expression to use for finding files."""

    subdirs = models.BooleanField(
        "Sub directories",
        default=False,
        help_text="Scan sub directories above the specified `location`",
    )
    """Scan sub directories above the specified `location`"""

    @property
    def relative_path(self) -> str:
        """The :attr:`location` relative to the catalog root location."""
        return osp.relpath(self.location, get_content_dir())


class Creator(models.Model):
    """a model for Creator of the dataset."""

    creator_name = models.SlugField(
        max_length=50, help_text="e.g. DOC/NOAA/NWS/NCEP"
    )
    """creator name e.g. DOC/NOAA/NWS/NCEP"""

    creator_url = models.SlugField(
        max_length=50, help_text="e.g. http://www.ncep.noaa.gov/"
    )
    """creator url e.g. http://www.ncep.noaa.gov/"""

    creator_email = models.SlugField(
        max_length=50,
        help_text="e.g. http://www.ncep.noaa.gov/mail_liaison.shtml",
    )
    """creator email e.g. http://www.ncep.noaa.gov/mail_liaison.shtml"""

    def __str__(self) -> str:
        """return creator name"""
        return self.creator_name


class Publisher(models.Model):
    """a model for Publisher of the dataset."""

    publisher_name = models.SlugField(
        max_length=50, help_text="e.g. UCAR/UNIDATA"
    )
    """publisher name e.g. UCAR/UNIDATA"""

    publisher_url = models.SlugField(
        max_length=50, help_text="e.g. http://www.unidata.ucar.edu/"
    )
    """publisher url e.g. http://www.unidata.ucar.edu/"""

    publisher_email = models.SlugField(
        max_length=50, help_text="e.g. support@unidata.ucar.edu"
    )
    """publisher email e.g. support@unidata.ucar.edu"""

    def __str__(self) -> str:
        """return publisher name"""
        return self.publisher_name


class Documentation(models.Model):
    """a model for Documentation of the dataset."""

    documentation_title = models.SlugField(
        max_length=50, help_text="e.g. NCEP Model documentation"
    )
    """documentation title e.g. NCEP Model documentation"""

    documentation_rights = models.SlugField(
        max_length=50, help_text="dataFormat e.g. Freely available"
    )
    """documentation rights e.g. Freely available"""

    documentation_link = models.SlugField(
        max_length=50,
        help_text="e.g. http://www.emc.ncep.noaa.gov/modelinfo/index.html",
    )
    """documentation link e.g. http://www.emc.ncep.noaa.gov/modelinfo/index.html"""

    def __str__(self) -> str:
        """return documentation title"""
        return self.documentation_title


class EnhancedCatalog(models.Model):
    """a model for enahancedCatalog in a thredds server."""

    id = models.UUIDField(
        primary_key=True,
        default=uuid.uuid4,
        editable=False,
        help_text="The UUID of the dataset.",
    )
    """The UUID of the dataset."""

    name = models.CharField(
        max_length=200, help_text="The name of the dataset"
    )
    """The name of the dataset"""

    catalog = models.ForeignKey(
        TDSCatalog,
        on_delete=models.CASCADE,
        help_text="The catalog that holds this entry.",
    )
    """The catalog that holds this entry."""

    class DataType(models.TextChoices):
        """Available data types for datasets."""

        grid = "Grid", "Grid"
        image = "Image", "Image"
        point = "Point", "Point"
        radial = "Radial", "Radial"
        station = "Station", "Station"
        swath = "Swath", "Swath"
        trajectory = "Trajectory", "Trajectory"

    class ServiceType(models.TextChoices):
        """Available service types for enahncedCatalog."""

        both = "both", "Compound"
        ncdods = "ncdods", "OpenDAP"
        httpserver = "HTTPServer", "HTTPServer"

    metadata_inherited = models.BooleanField(
        "",
        default=True,
        help_text="",
    )
    """Inherit metadata from parent catalog?"""

    service_type = models.CharField(
        choices=ServiceType.choices,
        default=ServiceType.both,
        max_length=10,
        help_text="Web Services of the dataset.",
    )
    """Web Services of the dataset."""

    authority = models.SlugField(
        max_length=150,
        help_text=("e.g : edu.ucar.unidata"),
    )
    """authority e.g. edu.ucar.unidata"""

    data_type = models.CharField(
        choices=DataType.choices,
        default=DataType.grid,
        max_length=10,
        help_text="Type of the dataset.",
    )
    """Type of the dataset."""

    data_format = models.SlugField(
        max_length=50, default="NetCDF", help_text="dataFormat e.g. NetCDF"
    )
    """dataFormat e.g. NetCDF"""

    creator = models.ForeignKey(
        Creator,
        on_delete=models.CASCADE,
        help_text="The creator details of dataset.",
        related_name="+",
    )
    """The creator details of dataset."""

    publisher = models.ForeignKey(
        Publisher,
        on_delete=models.CASCADE,
        help_text="The publisher details of dataset.",
        related_name="+",
    )
    """The publisher details of dataset."""

    documentation = models.ForeignKey(
        Documentation,
        on_delete=models.CASCADE,
        help_text="The documentation details of dataset.",
        related_name="+",
    )
    """The documentation details of dataset."""

    time_coverage_end = models.SlugField(
        max_length=100, help_text="e.g. present"
    )
    """time coverage end e.g. present"""

    time_coverage_duration = models.CharField(
        max_length=100, help_text="e.g. 14 days"
    )
    """time coverage duration e.g. 14 days"""

    xml = models.TextField(
        help_text="The rendered XML string of the dataset config.",
        default="",
    )
    """The rendered XML string of the dataset config."""

    def render_xml(self) -> str:
        """Render the catalog as xml."""
        return render_to_string(
            "tds_control/enhancedCatalog_template.xml",
            {"enhancedCatalog": self},
        )

    def __str__(self) -> str:
        """return the name of the enhancedCatalog"""

        return self.name


class EnhancedCatalogData(models.Model):
    """An entry in a thredds enhancedCatalog."""

    class Meta:
        """Meta class for EnhancedCatalogData."""

        abstract = True

    class DataType(models.TextChoices):
        """Available data types for datasets."""

        grid = "Grid", "Grid"
        image = "Image", "Image"
        point = "Point", "Point"
        radial = "Radial", "Radial"
        station = "Station", "Station"
        swath = "Swath", "Swath"
        trajectory = "Trajectory", "Trajectory"

    id = models.UUIDField(
        primary_key=True,
        default=uuid.uuid4,
        editable=False,
        help_text="The UUID of the dataset.",
    )
    """The UUID of the dataset."""

    name = models.CharField(
        max_length=200, help_text="The name of the dataset"
    )
    """The name of the dataset"""

    data_type = models.CharField(
        choices=DataType.choices,
        default=DataType.grid,
        max_length=10,
        help_text="Type of the dataset.",
    )
    """Type of the dataset."""

    enhanced_catalog = models.ForeignKey(
        EnhancedCatalog,
        on_delete=models.CASCADE,
        help_text="EnhancedCatalog that uses this netcdf file",
        null=True,
    )
    """EnhancedCatalog that uses this netcdf file"""

    def __str__(self) -> str:
        """return the name of the dataset"""

        return str(self.id)


class EnhancedDataset(EnhancedCatalogData):
    """A dataset representing one single file."""

    data_location = models.FilePathField(
        path=get_content_dir,
        recursive=True,
        max_length=10000,
        help_text="The location of the files that shall be used",
    )
    """The location of the files that shall be used"""

    @property
    def url_path(self) -> str:
        """The `urlPath` attribute for the catalog.

        The `urlPath` attribute is the :attr:`relative_path` of the dataset
        prepended with the catalog :attr:`~TDSCatalog.name`."""
        return (
            self.enhanced_catalog.catalog.name + "/" + self.relative_path  # type: ignore
        )

    @property
    def relative_path(self) -> str:
        """The :attr:`location` relative to the catalog root location."""
        return osp.relpath(self.data_location, get_content_dir())


class EnhancedDatasetScan(EnhancedCatalogData):
    """A datasetscan creating multiple datasets with a single entry."""

    # def only_numbers(value):
    #     """Check if the id contains only numbers."""
    #     if not isinstance(value, int) or not isinstance(value, float):
    #         raise ValidationError(
    #             "ID contains characters or is not float and int"
    #         )

    path = models.SlugField(
        max_length=50,
        validators=[MinLengthValidator(2)],
        help_text=(
            "The path of the dataset. The final path will be a combination of "
            "the catalog name and this string."
        ),
    )
    """The path of the dataset. The final path will be a combination of the catalog name and this string."""

    dataset_location = models.FilePathField(
        path=get_content_dir,
        recursive=True,
        allow_files=False,
        max_length=10000,
        allow_folders=True,
        help_text="The directory to scan.",
    )
    """The directory to scan."""

    data_type = models.CharField(  # type: ignore
        choices=BaseDataset.DataType.choices,
        default=BaseDataset.DataType.grid,
        null=True,
        blank=True,
        max_length=10,
        help_text="Type of the dataset.",
    )
    """Type of the dataset."""

    documentation_summary = models.CharField(
        max_length=10,
        help_text="e.g. NCEP North American Model : AWIPS 211 (Q) Regional - CONUS (Lambert Conformal).",
    )
    """documentation summary e.g. NCEP North American Model : AWIPS 211 (Q) Regional - CONUS (Lambert Conformal)."""

    geospatial_lat_min = models.FloatField(
        help_text="Southern boundary of the bounding box",
        validators=[
            validators.MinValueValidator(-90),
            validators.MaxValueValidator(90),
        ],
    )
    """geospatial lat min e.g. 0.0"""

    geospatial_lat_max = models.FloatField(
        help_text="Northern boundary of the bounding box",
        validators=[
            validators.MinValueValidator(-90),
            validators.MaxValueValidator(90),
        ],
    )
    """geospatial lat max e.g. 0.0"""

    geospatial_lon_min = models.FloatField(
        help_text="Eastern boundary of the bounding box",
        validators=[
            validators.MinValueValidator(-360),
            validators.MaxValueValidator(360),
        ],
    )
    """geospatial lon min e.g. 0.0"""

    geospatial_lon_max = models.FloatField(
        help_text="Western boundary of the bounding box, e.g. 103.78772",
        validators=[
            validators.MinValueValidator(-360),
            validators.MaxValueValidator(360),
        ],
    )
    """geospatial lon max e.g. 0.0"""

    addTimeCoverage_datasetNameMatchPattern = models.CharField(
        max_length=100,
        help_text="e.g. ([0-9]{4})([0-9]{2})([0-9]{2})([0-9]{2})_eta_211.nc$",
    )
    """addTimeCoverage datasetNameMatchPattern e.g. ([0-9]{4})([0-9]{2})([0-9]{2})([0-9]{2})_eta_211.nc$"""

    addTimeCoverage_startTimeSubstitutionPattern = models.CharField(
        max_length=100, help_text="e.g. $1-$2-$3T$4:00:00"
    )
    """addTimeCoverage startTimeSubstitutionPattern e.g. $1-$2-$3T$4:00:00"""

    addTimeCoverage_duration = models.CharField(
        max_length=100, help_text="e.g. 60 hours"
    )
    """addTimeCoverage duration e.g. 60 hours"""

    @property
    def relative_path(self) -> str:
        """The :attr:`location` relative to the catalog root location."""
        return osp.relpath(self.dataset_location, get_content_dir())


class EnhancedDatasetScanSubs(models.Model):
    """A mixin for EnhancedDatasetScanVariables and EnhancedDatasetScanFilters inlines."""

    id = models.UUIDField(
        primary_key=True,
        default=uuid.uuid4,
        editable=False,
        help_text="The UUID of the dataset.",
    )
    """The UUID of the dataset."""

    enhanced_catalog_sub = models.ForeignKey(
        EnhancedCatalog,
        on_delete=models.CASCADE,
        help_text="EnhancedCatalog that uses this netcdf file",
        null=True,
    )
    """EnhancedCatalog that uses this netcdf file"""

    @property
    def relative_path(self) -> str:
        """The :attr:`location` relative to the enhancedCatalog root location."""
        return str(self.id)


class EnhancedDatasetScanVariables(EnhancedDatasetScanSubs):
    """A enhanceddatasetscan creating multiple datasets with a single entry."""

    enhanced_catalog_datascan = models.ForeignKey(
        EnhancedDatasetScan,
        on_delete=models.CASCADE,
        help_text="The enhancedCatalog that holds this entry.",
    )
    """The enhancedCatalog that holds this entry."""

    variable_name = models.CharField(max_length=200, help_text="e.g. Z_sfc")
    """variable name e.g. Z_sfc"""

    veocabulary_name = models.CharField(
        max_length=200, help_text="e.g. geopotential_height"
    )
    """veocabulary name e.g. geopotential_height"""

    units = models.CharField(max_length=200, help_text="e.g. gp m")
    """units e.g. gp m"""

    presented_name = models.CharField(
        max_length=200, help_text="e.g. Geopotential height, gpm"
    )
    """presented name e.g. Geopotential height, gpm"""

    @property
    def relative_path(self) -> str:
        """The :attr:`location` relative to the catalog root location."""
        return str(self.presented_name)


class EnhancedDatasetScanFilters(EnhancedDatasetScanSubs):
    """A enhanceddatasetscan creating multiple datasets with a single entry."""

    wildcard = models.CharField(max_length=200, help_text="e.g. *eta_211.nc")
    """filter string e.g. *eta_211.nc"""
    enhanced_catalog_datascan = models.ForeignKey(
        EnhancedDatasetScan,
        on_delete=models.CASCADE,
        help_text="The enhancedCatalog that holds this entry.",
    )
    """The enhancedCatalog that holds this entry."""

    @property
    def relative_path(self) -> str:
        """The :attr:`location` relative to the catalog root location."""
        return str(self.wildcard)


# pre_save signals
@receiver(pre_save, sender=TDSCatalog)
@receiver(pre_save, sender=TDSServer)
@receiver(pre_save, sender=WMSConfig)
@receiver(pre_save, sender=AggregationDataset)
@receiver(pre_save, sender=SimpleDataset)
@receiver(pre_save, sender=DatasetScan)
@receiver(pre_save, sender=EnhancedCatalog)
def update_xml_field(
    sender,
    instance: Union[
        AggregationDataset, SimpleDataset, DatasetScan, EnhancedCatalog
    ],
    **kwargs,
):
    """Overwrite the xml field in AggregationDataset, SimpleDataset, DatasetScan, and EnhancedCatalog models."""
    instance.xml = instance.render_xml()


# post_save and post_delete signals
@receiver(post_delete, sender=AggregationDataset)
@receiver(post_delete, sender=SimpleDataset)
@receiver(post_delete, sender=DatasetScan)
@receiver(post_delete, sender=EnhancedCatalog)
@receiver(post_save, sender=AggregationDataset)
@receiver(post_save, sender=SimpleDataset)
@receiver(post_save, sender=DatasetScan)
@receiver(post_save, sender=EnhancedCatalog)
def sve_and_write_catalog(
    sender,
    instance: Union[
        AggregationDataset, SimpleDataset, DatasetScan, EnhancedCatalog
    ],
    **kwargs,
):
    """Write the xml field and save the catalog after a post_save or post_delete signal in AggregationDataset, SimpleDataset, DatasetScan, and EnhancedCatalog models."""
    instance.catalog.save()
    instance.catalog.write_xml()

    # for server in instance.catalog.tdsserver_set.all():
    #     try:
    #         server.restart_server()
    #     except Exception:
    #         pass


# post_save signals in inlines of EnhancedCatalog model
@receiver(post_save, sender=EnhancedDataset)
@receiver(post_save, sender=EnhancedDatasetScan)
def save_enhancedcatalog(
    sender,
    instance: Union[EnhancedDataset, EnhancedDatasetScan],
    **kwargs,
):
    """Overwrite the xml field in EnhancedCatalog model."""
    if instance.enhanced_catalog is not None:
        instance.enhanced_catalog.save()


# post_save signals in nested inlines of EnhancedDatasetScan model
@receiver(post_save, sender=EnhancedDatasetScanVariables)
@receiver(post_save, sender=EnhancedDatasetScanFilters)
def save_datascan_enhancedcatalog_components(
    sender,
    instance: Union[EnhancedDatasetScanVariables, EnhancedDatasetScanFilters],
    **kwargs,
):
    """Overwrite the xml field in nested inlines of EnhancedDatasetScan model."""
    if instance.enhanced_catalog_datascan is not None:
        instance.enhanced_catalog_datascan.save()


# post_save signals in inlines of Aggregation model
@receiver(post_save, sender=NetCDFFile)
@receiver(post_save, sender=NetCDFScan)
def save_aggregation_components(
    sender,
    instance: Union[NetCDFFile, NetCDFScan],
    **kwargs,
):
    """Overwrite the xml field in inlines of Aggregation model."""
    instance.aggregation.save()


# post_save signals in inline of TDSCatalog model
@receiver(post_save, sender=CatalogRef)
def save_catalogref(
    sender,
    instance: CatalogRef,
    **kwargs,
):
    """Overwrite the xml field in inline of TDSCatalog model."""
    instance.conn_catalog.save()


@receiver(post_save, sender=TDSCatalog)
def write_all_objects_tdscatalog_in_tdsconfiguration(
    sender, instance: TDSCatalog, created: bool, **kwargs
):
    """Write the xml field of the catalog after a post_save signal in TDSCatalog model."""
    instance.write_xml()
    for server in instance.tdsserver_set.all():
        if created:
            server.write_xml()
        # try:
        #     server.restart_server()
        # except:
        #     pass


# @receiver(pre_delete, sender=TDSCatalog)
# def delete_catalog_file(sender, instance: TDSCatalog, created: bool, **kwargs):
#     if osp.exists(instance.xml_path):
#         os.remove(instance.xml_path)
#     for server in instance.tdsserver_set.all():
#         server.write_xml()
#         server.restart_server()


# post_save signals in inline of TDSServer model
@receiver(post_save, sender=TDSServer)
def write_app_setting_and_threddsConfig(
    sender, instance: TDSServer, created: bool, **kwargs
):
    """write app_setting.py and threddsConfig.xml after a post_save signal in TDSServer model."""
    instance.write_tds_control_app_setting()

    instance.write_xml()
    # print("here")
    # try:
    #     print("here_try")
    #     instance.restart_server()
    # except:
    #     print("here_except")
    #     pass


# post_save signals in WMSConfig model
@receiver(post_save, sender=WMSConfig)
def write_configrations_of_wmsconfig(
    sender, instance: WMSConfig, created: bool, **kwargs
):
    """Write the xml field of the catalog after a post_save signal in WMSConfig model."""
    instance.write_xml()


# post_save signals in inlines of WMSConfig model
@receiver(post_save, sender=WMSStandardName)
@receiver(post_save, sender=WMSOverride)
def save_wmsconfig_components(
    sender,
    instance: Union[WMSStandardName, WMSOverride],
    **kwargs,
):
    """Overwrite the xml field in inlines of WMSConfig model."""
    if instance.wms is not None:
        instance.wms.save()


# post_save signals in nested inline of WMSOverride model
@receiver(post_save, sender=WMSVariable)
def save_wmsoverride_wmsconfing_components(
    sender,
    instance: WMSVariable,
    **kwargs,
):
    """Overwrite the xml field in nested inline of WMSOverride model."""
    if instance.category is not None:
        instance.category.save()


# m2m_changed signals in TDSServer model for catalogs
@receiver(m2m_changed, sender=TDSServer.catalogs.through)
def write_catalogs_in_threddsConfig(sender, instance: TDSServer, **kwargs):
    """Write the xml field of the catalog after a m2m_changed signal in TDSServer model."""
    instance.write_xml()
    # try:
    #     instance.restart_server()
    # except:
    #     pass
